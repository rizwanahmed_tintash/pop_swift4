//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/****************            Generic Definition         ****************/
struct BlobRecordUInt16 {
    var value               : UInt16 = 0
    var isNeedtoLogRecord   : Bool   = false
    var key                 :String
    init(_ recordValue: UInt16, _ isNeedToLog : Bool) {
        value = recordValue
        isNeedtoLogRecord = isNeedToLog
        key = ""
    }
}
struct BlobRecordUInt32 {
    var value               : UInt32 = 0
    var isNeedtoLogRecord   : Bool   = false
    var key                 :String
    init(_ recordValue: UInt32, _ isNeedToLog : Bool) {
        value = recordValue
        isNeedtoLogRecord = isNeedToLog
        key = ""
    }
}
struct BlobRecordUInt64 {
    var value               : UInt64 = 0
    var isNeedtoLogRecord   : Bool   = false
    var key                 :String
    init(_ recordValue: UInt64, _ isNeedToLog : Bool) {
        value = recordValue
        isNeedtoLogRecord = isNeedToLog
        key = ""
    }
}


struct BlobRecord<T> {
    var value               : T
    var isNeedtoLogRecord   : Bool   = false
    var key                 :String
    init(_ recordValue: T, _ isNeedToLog : Bool) {
        value = recordValue
        isNeedtoLogRecord = isNeedToLog
        key = ""
    }
}


let blobrecord1 = BlobRecordUInt16(20, false)
let blobrecord2 = BlobRecordUInt64(20, false)

let blobrecord3 = BlobRecord(blobrecord1.value, true)
let blobrecord4 = BlobRecord(blobrecord2.value, true)

/****************            Generic Function         ****************/
func addition<T: Numeric>(a: T, b: T) -> T
{
    return a + b
}


let a1:UInt32 = 20
let b1:UInt32 = 20
let c1 = addition(a: a1, b: b1)

let a2:UInt16 = 10
let b2:UInt16 = 18
let c2 = addition(a: a2, b: b2)


//func average<T: Numeric>(a: T, b: T) -> T
//{
//    let divider:T = 2
//    return (a + b) / 2
//}

/****************            Generic Types Constraints        ****************/
func subtraction<T: Numeric>(a: T, b: T) -> T
{
    return a - b
}


func checkEquality<T: Equatable>(a: T, b: T) -> Bool
{
    return a == b
}

func isLarger<T: Comparable>(a: T, b: T) -> Bool
{
    return a > b
}

/****************            Associated Types         ****************/

protocol WaveFormRecord {
    associatedtype Record
    var recordList: [Record] { get set}
    mutating func addRecord(record: Record)
}

struct WeeklyRecordInt16 : WaveFormRecord {
    var recordList: [BlobRecordUInt16]  = []
    mutating func addRecord(record: Record) {
        recordList.append(record)
    }
}

struct WeeklyRecordInt32 : WaveFormRecord {
    var recordList: [BlobRecordUInt32]  = []
    mutating func addRecord(record: Record) {
        recordList.append(record)
    }
}




struct WeeklyRecord<T> : WaveFormRecord {
    var recordList: [T]  = []
    mutating func addRecord(record: T) {
        recordList.append(record)
    }
}


var record1 = WeeklyRecord<BlobRecordUInt16>()
let blobrecord5 = BlobRecordUInt16(10, false)
let blobrecord6 = BlobRecordUInt16(20, true)
record1.addRecord(record: blobrecord1)
record1.addRecord(record: blobrecord5)
record1.addRecord(record: blobrecord6)

/****************            Generic Subscript        ****************/

struct DeviceList<T> {
    private var devices = [T]()
    public mutating func addDeviceID(device: T) {
        devices.append(device)
    }
    public func getDeviceAtIndex(index: Int) -> T? {
        if devices.count > index {
            return devices[index]
        } else {
            return nil
        }
    }
    public subscript(index: Int) -> T? {
        return getDeviceAtIndex(index: index)
    }
    public subscript<E: Sequence>(indices: E) -> [T]
        where E.Iterator.Element == Int {
            var result = [T]()
            for index in indices {
                result.append(devices[index])
            }
            return result
    }
}

var wristifyDeviceList = DeviceList<Int>()
wristifyDeviceList.addDeviceID(device:  13424)
wristifyDeviceList.addDeviceID(device:  23646)
wristifyDeviceList.addDeviceID(device:  37568)
wristifyDeviceList.addDeviceID(device:  11554)
wristifyDeviceList.addDeviceID(device:  57862)
wristifyDeviceList[2...4]
wristifyDeviceList[4]




/****************            Generic Subscript        ****************/


protocol List {
    associatedtype T
    subscript<E: Sequence>(indices: E) -> [T] where E.Iterator.Element == Int { get }
    mutating func add(_ item: T)
    func length() -> Int
    func get(at: Int) -> T?
}

private class BackendList<T> {
    private var items: [T] = []
    
    public init() {}
    private init(_ items: [T]) {
        self.items = items
    }
    
    public func add(_ item: T) {
        items.append(item)
    }
    public func length() -> Int {
        return items.count
    }
    public func get(at index: Int) -> T? {
        return items[index]
    }
    public func copy() -> BackendList<T> {
        return BackendList<T>(items)
    }
}

struct ArrayList<T>: List {
    private var items = BackendList<T>()
    public subscript<E: Sequence>(indices: E) -> [T]
        where E.Iterator.Element == Int {
            var result = [T]()
            for index in indices {
                if let item = items.get(at: index) {
                    result.append(item) }
            }
            return result
    }
    public mutating func add(_ item: T) {
        checkUniquelyReferencedInternalQueue()
        items.add(item)
    }
    public func length() -> Int {
        return items.length()
    }
    public func get(at index: Int) -> T? {
        return items.get(at: index)
    }
    mutating private func checkUniquelyReferencedInternalQueue() {
        if !isKnownUniquelyReferenced(&items) {
            print("Making a copy of internalQueue")
            items = items.copy()
        } else {
            print("Not making a copy of internalQueue")
        }
    }
}



var arrayList = ArrayList<Int>()
arrayList.add(1)
arrayList.add(2)
arrayList.add(3)

var newArrayList = arrayList
arrayList.add(4)
