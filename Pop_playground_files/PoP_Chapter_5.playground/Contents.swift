//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


class BlobRecordAUInt64 {
    var value               : UInt64
    var isNeedtoLogRecord   : Bool
    var key                 :String
    init(_ recordValue: UInt64, _ isNeedToLog : Bool, recordType: String) {
        value = recordValue
        isNeedtoLogRecord = isNeedToLog
        key = recordType
    }
}
class BlobRecordAUInt32 {
    var value               : UInt32
    var isNeedtoLogRecord   : Bool
    var key                 :String
    init(_ recordValue: UInt32, _ isNeedToLog : Bool, recordType: String) {
        value = recordValue
        isNeedtoLogRecord = isNeedToLog
        key = recordType
    }
}

class BlobRecordAUInt16 {
    var value               : UInt16
    var isNeedtoLogRecord   : Bool
    var key                 :String
    init(_ recordValue: UInt16, _ isNeedToLog : Bool, recordType: String) {
        value = recordValue
        isNeedtoLogRecord = isNeedToLog
        key = recordType
    }
}

class BlobRecord {
    var isNeedtoLogRecord   : Bool
    var key                 :String
    init(isNeedToLog : Bool, recordType: String) {
        isNeedtoLogRecord = isNeedToLog
        key = recordType
    }
}
class BlobRecordUInt16:BlobRecord {
    var value                 :UInt16
    init(_ recordValue: UInt16, _ isNeedToLog : Bool, recordType: String) {
        value = recordValue
        super.init(isNeedToLog: isNeedToLog, recordType: recordType)
    }
}
class BlobRecordUInt32:BlobRecord {
    var value                 :UInt32
    init(_ recordValue: UInt32, _ isNeedToLog : Bool, recordType: String) {
        value = recordValue
        super.init(isNeedToLog: isNeedToLog, recordType: recordType)
    }
}
class BlobRecordUInt64:BlobRecord {
    var value                 :UInt64
    init(_ recordValue: UInt64, _ isNeedToLog : Bool, recordType: String) {
        value = recordValue
        super.init(isNeedToLog: isNeedToLog, recordType: recordType)
    }
}

var records = [BlobRecord]()
var record1 = BlobRecordUInt16(10, false, recordType: "duration")
var record2 = BlobRecordUInt32(90, true, recordType:  "duration")
var record3 = BlobRecordUInt64(12, false, recordType: "duration")
var record4 = BlobRecordUInt16(45, true, recordType: "duration")
records.append(record1)
records.append(record2)
records.append(record3)
records.append(record4)

for (index, record) in records.enumerated() {
    if record is BlobRecordUInt16{
        print("record \(index) is BlobRecordUInt16")
    }
    else if record is BlobRecordUInt32{
        print("record \(index) is BlobRecordUInt32")
    }
    else if record is BlobRecordUInt64{
        print("record \(index) is BlobRecordUInt64")
    }
}

class Duration : BlobRecordUInt16 {
    
}

class Session : BlobRecordUInt16 {
    
}



enum TerrainType {
    case Land
    case Sea
    case Air
}

class Vehicle {
    fileprivate var vehicleTypes = [TerrainType]()
    fileprivate var vehicleAttackTypes = [TerrainType]()
    fileprivate var vehicleMovementTypes = [TerrainType]()
    
    fileprivate var landAttackRange = -1
    fileprivate var seaAttackRange = -1
    fileprivate var airAttackRange = -1
    
    fileprivate var hitPoints = 0
    
    func isVehicleType(type: TerrainType) -> Bool {
        return vehicleTypes.contains(type)
    }
    func canVehicleAttack(type: TerrainType) -> Bool {
        return vehicleAttackTypes.contains(type)
    }
    func canVehicleMove(type: TerrainType) -> Bool {
        return vehicleMovementTypes.contains(type)
    }
    func doLandAttack() {}
    func doLandMovement() {}
    
    func doSeaAttack() {}
    func doSeaMovement() {}
    
    func doAirAttack() {}
    func doAirMovement() {}
    
    func takeHit(amount: Int) { hitPoints -= amount }
    func hitPointsRemaining() -> Int { return hitPoints }
    func isAlive() -> Bool { return hitPoints > 0 ? true : false }
}

class Tank: Vehicle {
    override init() {
        super.init()
        vehicleTypes = [.Land]
        
        vehicleAttackTypes = [.Land]
        vehicleMovementTypes = [.Land]
        landAttackRange = 5
        
        hitPoints = 68
    }
    
    override func doLandAttack() {
        print("Tank Attack")
    }
    override func doLandMovement() {
        print("Tank Move") }
}

class Amphibious: Vehicle {
    override init() {
        super.init()
        vehicleTypes = [.Land, .Sea]
        vehicleAttackTypes = [.Land, .Sea]
        vehicleMovementTypes = [.Land, .Sea]
        
        landAttackRange = 1
        seaAttackRange = 1
        
        hitPoints = 25
    }
    override func doLandAttack() {
        print("Amphibious Land Attack")
    }
    override func doLandMovement() {
        print("Amphibious Land Move")
    }
    override func doSeaAttack() {
        print("Amphibious Sea Attack")
    }
    override func doSeaMovement() {
        print("Amphibious Sea Move")
    }
}

class Transformer: Vehicle {
    override init() {
        super.init()
        vehicleTypes = [.Land, .Sea, .Air]
        vehicleAttackTypes = [.Land, .Sea, .Air]
        vehicleMovementTypes = [.Land, .Sea, .Air]
        
        landAttackRange = 7
        seaAttackRange = 10
        airAttackRange = 12
        
        hitPoints = 75
    }
    
    override func doLandAttack() {
        print("Transformer Land Attack")
    }
    override func doLandMovement() {
        print("Transformer Land Move")
    }
    
    override func doSeaAttack() {
        print("Transformer Sea Attack")
    }
    override func doSeaMovement() {
        print("Transformer Sea Move")
    }
    
    override func doAirAttack() {
        print("Transformer Air Attack")
    }
    override func doAirMovement() {
        print("Transformer Air Move")
    }
}

class Infantry: Vehicle {
    override init() {
        super.init()
        vehicleTypes = [.Land]
        vehicleAttackTypes = [.Land]
        vehicleMovementTypes = [.Sea]
        
        landAttackRange = 1
        seaAttackRange = 1
        
        hitPoints = 25
    }
    override func doLandAttack() {
        print("Amphibious Land Attack")
    }
    override func doLandMovement() {
        print("Amphibious Land Move")
    }
}


var vehicles = [Vehicle]()

var vh1 = Amphibious()
var vh2 = Amphibious()
var vh3 = Tank()
var vh4 = Transformer()





