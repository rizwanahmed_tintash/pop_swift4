import UIKit

var str = "Hello, playground"

enum Designation:Int {
    case JuniorSoftwareEngineer = 2
    case SoftwareEngineer       = 4
    case SeniorSoftwareEngineer     = 6
}

protocol Developer {
    var name: String{get}
    var experience: Int{get set}
    var designation: Designation {get set}
}
extension  Developer {
    mutating func updateDesignation (experience: Int)  -> Designation {
        if experience <= 2 {
            designation = .JuniorSoftwareEngineer
        } else if experience > 2 && experience <= 4 {
            designation = .SoftwareEngineer
        } else if experience > 4  {
            designation = .SeniorSoftwareEngineer
        }
        return designation
    }
}

protocol GameDeveloper: Developer {
    var gameTech: [String] {get}
}

protocol IOSDeveloper: Developer {
    var iosTech: [String] {get}
}

protocol AndroidDeveloper: Developer {
    var androidTech: [String] {get}
}

protocol BackendDeveloper: Developer {
    var backendTech: [String] {get}
}


struct IOSDev:  IOSDeveloper {
    var name: String = ""
    
    var iosTech: [String] = ["native"]
    var experience: Int = 3
    var designation: Designation = .SoftwareEngineer
}

struct IOSGameDev: GameDeveloper, IOSDeveloper {
    var name: String = ""
    var gameTech: [String] = ["Unity", "cocos2d-x"]
    var iosTech: [String] = ["native"]
    var experience: Int = 3
    var designation: Designation = .SoftwareEngineer
}

struct TPM:  IOSDeveloper,AndroidDeveloper,BackendDeveloper  {
    var name: String = ""
    var androidTech: [String] =  ["native", "kotlin"]
    var backendTech: [String] = ["python", "nodejs"]
    var iosTech: [String] = ["native"]
    var experience: Int = 7
    var designation: Designation = .SeniorSoftwareEngineer
}


var developers = [Developer]()

var dev1 = IOSDev()
var dev2 = IOSGameDev()
var dev3 = TPM()
var dev4 = IOSDev(name:"riz", iosTech: ["react"], experience: 5, designation: .SeniorSoftwareEngineer)
developers.append(dev1)
developers.append(dev2)
developers.append(dev3)
developers.append(dev4)

for (index, dev) in developers.enumerated() {
    if let iosDev = dev as? IOSDev {
        print("\(iosDev.name) is a IOSDev")
    }
    if let iosGameDev = dev as? IOSGameDev {
        print("\(iosGameDev.name) is a IOSGameDev")
    }
    if let tpm = dev as? TPM {
        print("\(tpm.name) is a TPM")
    }
}

for (index, dev) in developers.enumerated() where dev is IOSDev {
    let developer = dev as! IOSDev
     print("\(developer.name) is a TPM")
}



func updateDesignation<T: Developer>(dev: inout T) {
    dev.updateDesignation(experience: 10)
}
print("\(dev4.name) have experience of \(dev4.experience) years")
updateDesignation(dev: &dev4)

print("Now \(dev4.name) have experience of \(dev4.experience) years")














