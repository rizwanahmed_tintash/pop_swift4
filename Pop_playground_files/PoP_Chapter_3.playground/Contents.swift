//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
/****************            Definition of Extension         ****************/
//extension UIColor {
//    struct GradientColor {
//        static var topWarmLightColor = UIColor(red: 184.0 / 255.0, green: 23.0 / 255.0, blue: 142.0 / 255.0, alpha: 1.0)
//        static var bottomWarmLightColor = UIColor(red: 236.0 / 255.0, green: 0.0 / 255.0, blue: 140.0 / 255.0, alphaa: 1.0)
//    }
//}


extension Date {
    func dayNumberOfWeek() -> Int? {
        return Calendar.current.dateComponents([.weekday], from: self).weekday
    }
}

/****************            Computed Properties         ****************/
extension Date {
    var age: Int {
        return (Calendar.current as NSCalendar).components(.year, from: self, to: Date(), options: []).year!
    }
}

let date = Date()
print("Day of the week is \(date.dayNumberOfWeek()!)")
date.dayNumberOfWeek()
print("Day of the week is \(date.age)")

/****************            Protocol Extension         ****************/

protocol BlobRecordUInt32 {
    var value               : UInt32 {get set}
    var isNeedtoLogRecord   : Bool {get set}
    var key                 :String {get set}
    init( recordValue: UInt32,  isNeedToLog : Bool)
    
}
extension BlobRecordUInt32 {
    func getCount() -> Int{
        if (self.isNeedtoLogRecord == true) {
            return 1
        } else { return 0 }
        
    }
}

struct Duration :BlobRecordUInt32 {
    var value: UInt32
    var isNeedtoLogRecord :Bool
    var key: String
    init( recordValue: UInt32,  isNeedToLog : Bool){
        value = recordValue
        key = "duration"
        isNeedtoLogRecord = isNeedToLog
    }
}

struct Session :BlobRecordUInt32 {
    var value: UInt32
    var isNeedtoLogRecord :Bool
    var key: String
    init( recordValue: UInt32,  isNeedToLog : Bool){
        value = recordValue
        key = "session"
        isNeedtoLogRecord = isNeedToLog
    }
}

var dataArray : [BlobRecordUInt32] = []
let session1 = Session(recordValue: 10, isNeedToLog: true)
let session2 = Session(recordValue: 0, isNeedToLog: false)
let duration1 = Duration(recordValue: 0, isNeedToLog: false)
let duration2 = Duration(recordValue: 12, isNeedToLog: true)
dataArray.append(session1)
dataArray.append(session2)
dataArray.append(duration1)
dataArray.append(duration2)

var commulativeDuration:UInt32 = 0
var commulativeSession:UInt32   = 0
var totalDuarationCount = 0
var totalSesssionCount  = 0

for data in dataArray {
    var durationArr:  [Duration] = []
    if let duration =  data as? Duration {
        durationArr.append(duration)
        commulativeDuration = commulativeDuration + duration.value
        totalDuarationCount = totalDuarationCount + duration.getCount()
    }
}

for session in dataArray {
    if session is Session {
        print("session value is : \(session.value)")
        commulativeSession = commulativeSession + session.value
        totalSesssionCount = totalSesssionCount + session.getCount()
    }
}

//let baseline = Int(Int(commulativeDuration) / totalDuarationCount)

/****************            Text Validation         ****************/

protocol TextValidation {
    var regFindMatchString: String {get}
    var validationMessage: String {get}
}

extension TextValidation {
    var regMatchingString: String {
        get {
            return regFindMatchString + "$"
        }
    }
    
    func isValidateString(str: String) -> Bool {
        if let _ = str.range(of: regMatchingString, options:
            .regularExpression){
            return true
        } else {
            return false
        }
    }
    
    func getMatchingString(str: String) -> String? {
        if let newMatch = str.range(of: regFindMatchString, options:
            .regularExpression){
            return str.substring(with: newMatch)
        } else {
            return nil
        }
    }
}

class NameValidation: TextValidation {
    let regFindMatchString = "^[\\s?[a-zA-Z\\-_\\s]]{0,20}"
    let validationMessage = "Name can only contain Alpha Caharacters"
}
var userDisplayName = "RizwanAhmed"
var validation = NameValidation()
print(validation.isValidateString(str: userDisplayName))
let validName = validation.getMatchingString(str: userDisplayName)


/****************            Equatable protocol         ****************/


struct WaveformRecord {
    var value: UInt32
    var isNeedtoLogRecord :Bool
    var key: String
}

extension WaveformRecord: Equatable {
    static func ==(lhs: WaveformRecord, rhs: WaveformRecord) -> Bool {
        return lhs.value == rhs.value &&
            lhs.isNeedtoLogRecord == rhs.isNeedtoLogRecord &&
            lhs.key == rhs.key
    }
}

let minUsage1 = WaveformRecord(value: 20, isNeedtoLogRecord: true, key: "duration")
let minUsage2 = WaveformRecord(value: 20, isNeedtoLogRecord: true, key: "duration")

print("Is duration match \(minUsage1 == minUsage2)")
let sessionCount1 = WaveformRecord(value: 20, isNeedtoLogRecord: true, key: "session")
let sessionCount2 = WaveformRecord(value: 20, isNeedtoLogRecord: true, key: "session")
print("Is session match \(sessionCount1 == sessionCount2)")

print("Is session duration match \(minUsage1 == sessionCount1)")


