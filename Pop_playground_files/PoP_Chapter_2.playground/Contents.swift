//: Playground - noun: a place where people can play

import UIKit
import CoreBluetooth
var str = "Hello, playground"


import Foundation


/****************        CLASS         ****************/
class DeviceData2 {
    var waveform0Min       : UInt32
    var waveform1Min       : UInt32
    var waveform2Min       : UInt32
    var waveform3Min       : UInt32
    
    init(waveform0Min: UInt32, waveform1Min: UInt32, waveform2Min: UInt32, waveform3Min: UInt32) {
        self.waveform0Min = waveform0Min
        self.waveform1Min = waveform1Min
        self.waveform2Min = waveform2Min
        self.waveform3Min = waveform3Min
    }
    func getWarmingMinutes () -> UInt32 {
        let minuteUsage  = waveform2Min + waveform3Min
        return minuteUsage
    }
    func getCoolingMinutes () -> UInt32 {
        let minuteUsage  = waveform0Min + waveform1Min
        return minuteUsage
    }
}

/****************        STRUCTURE         ****************/
struct DeviceData {
    var waveform0Min       : UInt32
    var waveform1Min       : UInt32
    var waveform2Min       : UInt32
    var waveform3Min       : UInt32
    
    func getCoolingMinutes () -> UInt32 {
        let minuteUsage  = waveform0Min + waveform1Min
        return minuteUsage
    }
    
    func getWarmingMinutes () -> UInt32 {
        let minuteUsage  = waveform2Min + waveform3Min
        return minuteUsage
    }
}

/****************        ACCESS CONTROL         ****************/
public class DeviceData3 { //it can be accesible anywhere in module (target)
    private var waveform0Min        : UInt32 //its accesible within class
    fileprivate var waveform1Min    : UInt32 //its accesible within source file
    
    init(waveform0Min: UInt32, waveform1Min: UInt32, waveform2Min: UInt32, waveform3Min: UInt32) {
        self.waveform0Min = waveform0Min
        self.waveform1Min = waveform1Min
    }
}

extension DeviceData3 {
    func getCoolingMinutes () -> UInt32 {
        // usage of fileprivate and private variable
        let minuteUsage  = waveform0Min + waveform1Min
        return minuteUsage
    }
}

/****************        ENUMERATION         ****************/


enum DeviceMode: UInt8 {
    case standard       = 0x00
    case coolingOnly    = 0x01
    case warmingOnly    = 0x02
    
    var code: UInt8 {
        return rawValue
    }
}

let deviceMode = DeviceMode(rawValue: 1)
enum NotificationChannel {
    case LEGACY, BETA, IOS
    
    var topic: String {
        switch self {
        case .LEGACY: return "/topics/firmwareUpdate"
        case .BETA: return "/topics/firmwareUpdateBeta"
        case .IOS: return "/topics/firmwareUpdateIos"
        }
    }
    
    var status: String {
        switch self {
        case .LEGACY: return "beta_status"
        case .BETA: return "beta_status"
        case .IOS: return "ios_status"
        }
    }
}


/****************        TUPLES         ****************/

func checkFirmwareCompatible(installedFirmware: Int , availabeFirmware: Int ) -> (firmwareToInstall: Int, isNeedtoForceUpdate: Bool) {
    var isNeedtoForceUpdate = false
    var firmwareToInstall = availabeFirmware
    if installedFirmware >= 18 {
        isNeedtoForceUpdate  = true
        firmwareToInstall = 19
    }
    return (firmwareToInstall, isNeedtoForceUpdate)
}

/****************            VALUE TYPE AND REFREBCE TYPE         ****************/
struct UserA {
    var firstName:String
    var lastName:String
    func getFullName() -> String {
        return "\(firstName) \(lastName)"
    }
}

func removeLastName(user: UserA) {
    var user = user
    user.lastName = ""
}
var user = UserA.init(firstName: "Rizwan", lastName: "Ahmed")
print("USER name before calling removeLastName is :\(user.getFullName())")
removeLastName(user: user)
print("USER name after calling removeLastName is :\(user.getFullName())")


class UserB {
    var firstName:String
    var lastName:String
    init(first_name: String, last_name: String) {
        firstName = first_name
        lastName  = last_name
    }
    func getFullName() -> String {
        return "\(firstName) \(lastName)"
    }
}

func removeLastName(user: UserB) {
    user.lastName = ""
}

var userB = UserB.init(first_name: "Rizwan", last_name: "Ahmed")
print("USER name before calling removeLastName is :\(userB.getFullName())")
removeLastName(user: userB)
print("USER name after calling removeLastName is :\(userB.getFullName())")

/****************            RECURSIVE         ****************/
class WristifyDevice {
    var id: String
    var next: WristifyDevice?
    init(id: String) {
        self.id = id
    }
}
var fist     = WristifyDevice(id: "489347424")
var second   = WristifyDevice(id: "o387389434")
var third   = WristifyDevice(id: "dshhiuy73")
fist.next = second
second.next = third

/****************            INHERITANCE FOR REFERNCE TYPE          ****************/


class UserBasicInfo {
    var firstName       : String = ""
    var lastName        : String = ""
    var email           : String = ""
    var birthdate       : Date
    var isGuestUser     : Bool
    init(birthdate: Date, isGuestUser: Bool) {
        self.birthdate = birthdate
        self.isGuestUser = isGuestUser
    }
    func getFullName()  -> String {
        return "\(firstName) + \(lastName)"
    }
    func getBirthYear() -> Int {
        let year = Calendar.current.component(.year, from: birthdate)
        return year
    }
}

class UserInfo: UserBasicInfo {
    var gender  : String = ""
    var height  : String = ""
    var weight  : String = ""
}

/****************            INHERITANCE Example from  WristifyCharacteristicsHandler code snipped         ****************/
class BaseCharacteristicsHandler {
    internal var peripheral : CBPeripheral?
    internal func readCharacteristic(for characteristic: CBCharacteristic?) -> Bool {
        guard characteristic != nil && peripheral != nil else {
            return false
        }
        self.peripheral?.readValue(for: characteristic!)
        return true
    }
}
class WristifyCharacteristicsHandler : BaseCharacteristicsHandler {
    //MARK:- Handler Variable
    static let sharedManager = WristifyCharacteristicsHandler()
    var batteryStatusCharacteristic         : CBCharacteristic?
    fileprivate var wristifyService         : CBService?
}

/****************            DYNAMIC DISPATCH EXAMPLE         ****************/
extension WristifyCharacteristicsHandler {
    final func readBatterStatus () -> Bool    {
        return self.readCharacteristic(for: self.batteryStatusCharacteristic)
    }
}
