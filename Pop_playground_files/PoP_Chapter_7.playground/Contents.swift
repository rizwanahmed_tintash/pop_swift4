import UIKit

var str = "Hello, playground"

/**************                 Singleton Pattern                **************/
class DataManager {
    //declare it static constant, so that only one instance will exist throughout the lifecycle of the application.
    static let sharedInstance = DataManager()
    var numberOfREsources = 0
    //private initiator will restrcict exteranal code from creating another instance of the class
    private init() {}
}

var dm1 = DataManager.sharedInstance
var dm2 = DataManager.sharedInstance
var dm3 = DataManager.sharedInstance
dm1.numberOfREsources = 7
print(dm1.numberOfREsources)
print(dm2.numberOfREsources)
print(dm3.numberOfREsources)
dm3.numberOfREsources = 8
print(dm1.numberOfREsources)
print(dm2.numberOfREsources)
print(dm3.numberOfREsources)


/**************                 Builder Pattern                **************/
struct RecordData {
    var waveformNumber: Int
    var duration: Int
    var numberOfsession: Int
    var isCooling: Bool = true
    var isExtended: Bool
    init(waveformNumber: Int, duration: Int, numberOfsession: Int, isCooling: Bool, isExtended: Bool) {
        self.waveformNumber = waveformNumber
        self.duration = duration
        self.numberOfsession = numberOfsession
        self.isCooling = isCooling
        self.isExtended = isExtended
    }
}

var waveform1 = RecordData(waveformNumber: 1, duration: 10, numberOfsession: 3, isCooling: true, isExtended: true)
var waveform2 = RecordData(waveformNumber: 2, duration: 10, numberOfsession: 3, isCooling: false, isExtended: false)


/**************               Builder method 1                **************/
protocol Record {
    var waveformNumber: Int {get set}
    var duration: Int {get set}
    var numberOfsession: Int {get set}
    var isCooling: Bool {get set}
    var isExtended: Bool {get set}
    
}



struct CoolingWaveform :Record {
    var waveformNumber: Int
    var duration: Int
    var numberOfsession: Int
    var isCooling: Bool = true
    var isExtended: Bool
    
}


struct CoolingDefaultWaveform :Record {
    var waveformNumber = 0
    var duration: Int
    var numberOfsession: Int
    var isCooling: Bool = true
    var isExtended: Bool
    
}

struct CoolingExtendedWaveform :Record {
    var waveformNumber = 1
    var duration: Int
    var numberOfsession: Int
    var isCooling: Bool = true
    var isExtended: Bool
    
}



struct WarmingDefaultWaveform :Record {
    var waveformNumber = 2
    var duration: Int
    var numberOfsession: Int
    var isCooling: Bool = false
    var isExtended: Bool
    
}

struct WarmingExtendedWaveform :Record {
    var waveformNumber = 3
    var duration: Int
    var numberOfsession: Int
    var isCooling: Bool = true
    var isExtended: Bool
    
}


struct WaveformRecord  {
    var waveformNumber: Int
    var duration: Int
    var numberOfsession: Int
    var isCooling: Bool
    var isExtended: Bool
    init(record:Record) {
        self.waveformNumber = record.waveformNumber
        self.duration = record.duration
        self.numberOfsession = record.numberOfsession
        self.isCooling = record.isCooling
        self.isExtended = record.isExtended
    }
    func printWaveformProperties() {
        print("waveformNumber:\(waveformNumber)")
        print("duration:\(duration)")
        print("numberOfsession:\(numberOfsession)")
        print("isCooling:\(isCooling)")
        print("isExtended:\(isExtended)")
    }
}

/**************               Builder method 2                **************/
struct WavformRecordData {
    var waveformNumber  = 0
    var duration        = 0
    var numberOfsession = 0
    var isCooling       = true
    var isExtended      = false
    mutating func setWaveformNumber(waveform: Int) {self.waveformNumber = waveform}
    mutating func setDuration(duration: Int) {self.duration = duration}
    mutating func setNumberOfsession(numberOfsession: Int) {self.numberOfsession = numberOfsession}
    mutating func setIsCooling(isCooling: Bool) {self.isCooling = isCooling}
    mutating func setIsExtended(isExtended: Bool) {self.isExtended = isExtended}
    func wavformFor(waveformNumber : Int) ->  RecordData {
        return RecordData(waveformNumber: waveformNumber, duration: duration, numberOfsession: numberOfsession, isCooling: isCooling, isExtended: isExtended)
    }
}

/**************                 Factory Pattern                **************/
protocol BlobRecordUInt32 {
    var value               : UInt32 {get set}
    var isNeedtoLogRecord   : Bool {get set}
    var key                 :String {get set}
    init( recordValue: UInt32,  isNeedToLog : Bool)
    
}
extension BlobRecordUInt32 {
    func getCount() -> Int{
        if (self.isNeedtoLogRecord == true) {
            return 1
        } else { return 0 }
        
    }
}

struct Duration :BlobRecordUInt32 {
    var value: UInt32
    var isNeedtoLogRecord :Bool
    var key: String
    init( recordValue: UInt32,  isNeedToLog : Bool){
        value = recordValue
        key = "duration"
        isNeedtoLogRecord = isNeedToLog
    }
}

struct Session :BlobRecordUInt32 {
    var value: UInt32
    var isNeedtoLogRecord :Bool
    var key: String
    init( recordValue: UInt32,  isNeedToLog : Bool){
        value = recordValue
        key = "session"
        isNeedtoLogRecord = isNeedToLog
    }
}

func getRecord (isDuration: Bool , for value: UInt32, isNeedtoLogRecord: Bool ) ->BlobRecordUInt32 {
    if isDuration {
        return Session(recordValue: value, isNeedToLog: isNeedtoLogRecord)
    } else {
        return Duration(recordValue: value, isNeedToLog: isNeedtoLogRecord)
    }
}


/**************               Structural Design Patterns                **************/
/**************               Structural Patterns                **************/
