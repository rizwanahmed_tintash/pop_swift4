//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


protocol UserBasicInfo {
    var firstName       : String {get set}  //read and write property of Protocol
    var lastName        : String {get set}  //read and write property of Protocol
    var email    : String {get set}  //read and write property of Protocol
    var birthdate       : Date {get set}    //read and write property of Protocol
    var isGuestUser     : Bool { get}       //read only property of Protocol
    
    func getFullName()  -> String           //method name of Protocol
    func getBirthYear() -> Int              //method name of Protocol
}

protocol UserInfo: UserBasicInfo {
    var gender  : String {get set}
    var height  : String {get set}
    var weight  : String {get set}
}


struct UserA: UserBasicInfo {
    var firstName = ""
    var lastName = ""
    var email = ""
    var birthdate = Date()
    var isGuestUser = true
    
    func getFullName() -> String {
        return "(firstName) (lastName)"
    }
    
    func getBirthYear() -> Int {
        let year = Calendar.current.component(.year, from: birthdate)
        return year
    }
}


struct UserB: UserInfo {
    
    var firstName = ""
    var lastName = ""
    var email = ""
    var birthdate = Date()
    var isGuestUser = true
    
    var gender = ""
    var height = ""
    var weight = ""
    
    func getFullName() -> String {
        return "(firstName) (lastName)"
    }
    
    func getBirthYear() -> Int {
        let year = Calendar.current.component(.year, from: birthdate)
        return year
    }
}


protocol UserOnBoardingInfo {
    var firstName       : String {get set}
    var lastName        : String {get set}
    var email    : String {get set}
    var birthdate       : Date {get set}
    
    func getFullName()  -> String
    func getBirthYear() -> Int
}

protocol UserSettingPageInfo {
    var gender  : String {get set}
    var height  : String {get set}
    var weight  : String {get set}
}

struct User: UserOnBoardingInfo, UserSettingPageInfo {
    
    var firstName = ""
    var lastName = ""
    var email = ""
    var birthdate = Date()
    
    var gender = ""
    var height = ""
    var weight = ""
    
    func getFullName() -> String {
        return "(firstName) (lastName)"
    }
    
    func getBirthYear() -> Int {
        let year = Calendar.current.component(.year, from: birthdate)
        return year
    }
}

// PROTOCOL AS TYPE


struct WaveformRecord {
    var duration:               BlobRecordUInt32
    var isRecordUsed:           Bool
}



//func parseBlobV1Record(data:Data) -> WaveformRecord{
//    var startIndx = 10
//    var value:UInt32  = 20
//    var duration =  BlobRecordUInt32(recordValue: value, isNeedToLog: true)
//    var record = WaveformRecord(duration: duration, isRecordUsed: false)
//    return record
//}

protocol BlobRecordUInt32 {
    var value               : UInt32 {get set}
    var isNeedtoLogRecord   : Bool {get set}
    var key                 :String {get set}
    init( recordValue: UInt32,  isNeedToLog : Bool)
}

struct Duration :BlobRecordUInt32 {
    var value: UInt32
    var isNeedtoLogRecord :Bool
    var key: String
    init( recordValue: UInt32,  isNeedToLog : Bool){
        value = recordValue
        key = "duration"
        isNeedtoLogRecord = isNeedToLog
    }
}

struct Session :BlobRecordUInt32 {
    var value: UInt32
    var isNeedtoLogRecord :Bool
    var key: String
    init( recordValue: UInt32,  isNeedToLog : Bool){
        value = recordValue
        key = "session"
        isNeedtoLogRecord = isNeedToLog
    }
}

var dataArray : [BlobRecordUInt32] = []
let session1 = Session(recordValue: 10, isNeedToLog: true)
let session2 = Session(recordValue: 0, isNeedToLog: false)
let duration1 = Duration(recordValue: 0, isNeedToLog: false)
let duration2 = Duration(recordValue: 12, isNeedToLog: true)
dataArray.append(session1)
dataArray.append(session2)
dataArray.append(duration1)
dataArray.append(duration2)


for data in dataArray {
    var durationArr:  [Duration] = []
    if let duration =  data as? Duration {
        durationArr.append(duration)
    }
}


for session in dataArray {
    if session is Session {
        print("session value is : \(session.value)")
    }
}


protocol WaveformRecord2 {
    associatedtype BlobRecord
    var duration: BlobRecord {get set}
}

struct DeviceUsage : WaveformRecord2 {
    var duration: UInt32
}

struct AgregatedData : WaveformRecord2 {
    var duration: UInt16
}





